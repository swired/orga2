global templosClasicos
global cuantosTemplosClasicos
extern malloc


;########### SECCION DE TEXTO (PROGRAMA)
section .text

templosClasicos:

; [rdi] = ptr a templos
; [rsi] = tamano del arreglo
; r8 = esTemploClasico?
; rax = ptr a arreglo de clasicos

;prologo
push rbp
mov rbp, rsp
push rdi
push rsi  ; alineada a 16

call cuantosTemplosClasicos
cmp rax, 0
jz .null

;preparo para pedir memoria
mov dl, 24 ; tamano struct
mul dl
mov rdi, rax ;rdi = CantClasicos * 24
call malloc

; fin pedido de memoria

pop rsi
pop rdi
push rax ; guardo el ptr a clasicos
sub rsp, 8

.ciclo:

cmp rsi, 0
jz .fin
call esTemploClasico
cmp r8, 1
jnz .false
mov dl, byte [rdi] ; rdx columna larga
mov rcx, [rdi+8] ; rcx ptr a nombre
mov r9b, byte [rdi+16] ; r9 columna corta

mov byte [rax], dl
mov [rax+8], rcx
mov byte [rax+16], r9b  
add rax, 24

.false:
add rdi, 24
sub rsi, 24
jmp .ciclo

.fin:
add rsp, 8
pop rax
ret

.null:
ret

esTemploClasico:

mov r8b, byte [rdi+16] ; r8b <- columna corto
sal r8, 1 
add r8, 1 ; r8 = 2n + 1
cmp r8b , byte [rdi]
jnz .false
mov r8, 1
ret

.false:
mov r8, 0
ret


cuantosTemplosClasicos:


; [rdi] = ptr a arreglo de templos
; [rsi] = tamano del arreglo
; [rax] = contador de templos clasicos
; [r8] = esTemploClasico

mov rax, 0

.ciclo:

cmp rsi, 0
jz .fin	; si termine
call esTemploClasico
cmp r8 , 1
jnz .avanzar
inc rax

.avanzar:

add rdi, 24
sub rsi, 24
jmp .ciclo

.fin:
ret
